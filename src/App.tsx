import * as React from 'react'

import { UploadPage } from './+app/UploadPage'

class App extends React.Component {
  render() {
    return (
      <div className="filesharoo-app">
        <header>
          <h3>fileshar.oo</h3>
        </header>
        <section>
          <UploadPage />
        </section>
        <footer>
          made in india - by nullcorp
        </footer>
      </div>
    )
  }
}

export default App
