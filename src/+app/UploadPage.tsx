import * as React from 'react'
import { connect } from 'react-redux'
import { Classes, Intent, Tag } from '@blueprintjs/core'

import { RootState, Dispatch } from '../store'
import { actionCreators as filesActionCreators, State as filesState } from '../store/files'

interface Props {
    error: filesState['error']
    expiresAt: filesState['expiresAt']
    dispatch: Dispatch
    hash: filesState['hash']
    uploadingFile: filesState['uploadingFile']
    uri: filesState['uri']
}
interface State {
    file: File | null
}

type InputEvent = React.ChangeEvent<HTMLInputElement>

class UploadPageComponent extends React.Component<Props, State> {
    state: State = {
        file: null
    }

    onAddFile = ({ target }: InputEvent) => this.setState({ file: target.files && target.files[0] })

    onUploadFile = () => this.props.dispatch(filesActionCreators.uploadFile({ file: this.state.file! }))

    uploadingMessage() {
        return (
            <div className="spinner-wrapper">
                <div className="spinner"></div>
            </div>
        )
    }

    uriError() {
        const error = this.props.error
        return (
            <div className="filesharoo-message">
                <div>filesharoo tried to understand your file, he really did! but for some reason he just could not
                concentrate. he decided that either the file was evil or that he had been cursed by demonic gods
                living deep in the internal servers. he decided to go to the village psychic for help but found him
                drunk and repeating the words
                "<Tag className={Classes.MINIMAL} intent={Intent.WARNING}>{error}</Tag>"
                .</div>
                <br />
                <div>maybe you could try to refresh the page re-upload the file to purge the evil that has taken
                root here?</div>
            </div>
        )
    }

    uriMessage() {
        const time = this.props.expiresAt!.toDateString() + ' ' + this.props.expiresAt!.toLocaleTimeString()
        return (
            <div className="filesharoo-message">
                <div> filesharoo meditated on your file and and found that your file is a question
            and its answer is {this.props.hash}. Now that he fully understands the role your
            file plays in maintaining the balance of the universe, he will replay your file
            whenever you visit <a href={this.props.uri}>{this.props.uri}</a>.</div>
                <br />
                <div>However he is rather forgetful and will no longer remember your file after
                <Tag className={Classes.MINIMAL} intent={Intent.SUCCESS}>{time}</Tag>.</div>
            </div>
        )
    }

    render() {
        const section = this.props.uploadingFile
            ? this.uploadingMessage()
            : this.props.uri && this.uriMessage() || this.props.error && this.uriError()
        return (
            <div className="upload-page">
                <div>
                    <input className="pt-input" onChange={this.onAddFile} type="file" />
                    <button className="pt-button" disabled={this.props.uploadingFile} onClick={this.onUploadFile}>
                        Upload
                    </button>
                </div>
                {section}
            </div>
        )
    }
}

const mapStateToProps = (state: RootState) => ({
    error: state.files.error,
    expiresAt: state.files.expiresAt,
    hash: state.files.hash,
    uploadingFile: state.files.uploadingFile,
    uri: state.files.uri
})

const mapDispatchToProps = (dispatch: Dispatch) => ({ dispatch })

export const UploadPage = connect(mapStateToProps, mapDispatchToProps)(UploadPageComponent)
