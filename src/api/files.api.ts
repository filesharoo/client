import { Observable } from 'rxjs/Observable'

import { baseURI } from '../config'

export interface FileUploadPayload {
    file: File
}

export interface FileUploadResponse {
    expiresAt: string
    hash: string
    uri: string
}

const maxSize = 2 * 1024 * 1024

export function upload({ file }: FileUploadPayload): Observable<FileUploadResponse> {
    if (!file) {
        return Observable.throw(new Error('file not present'))
    }
    if (file.size > maxSize) {
        return Observable.throw(new Error('file size exceeds 2MiB'))
    }
    const body = new FormData()
    body.append('file', file)
    const url = `${baseURI}/api/upload`
    return Observable.ajax({ body, crossDomain: true, method: 'post', url })
        .map(({ response }) => response.data as FileUploadResponse)
        .map(data => ({ ...data, uri: `${baseURI}/files/${data.uri}` }))
        .catch(data => {
            const error = data.response && data.response.error || data.message
            return Observable.throw(new Error(error))
        })
}