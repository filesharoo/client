const baseUrl = 'http://localhost:3001'
export type IHttpMethod = 'DELETE' | 'GET' | 'PATCH' | 'POST' | 'PUT'

const headers = new Headers()
headers.set('Content-Type', 'application/json; charset=utf-8')

export default async function<T>(method: IHttpMethod, url: string, payload?: Object) {
    const token = localStorage.token
    if (token) {
        headers.set('Authorization', `Bearer ${token}`)
    }
    const body = JSON.stringify(payload)
    const mode: 'cors' = 'cors'
    const options = { body, mode, headers, method }
    const response = await fetch(`${baseUrl}/${url}`, options)
    const json = await response.json()
    if (json.error) {
        throw new Error(json.error.message)
    }
    if (json.token) {
        localStorage.token = token
    }
    return json.data as T
}
