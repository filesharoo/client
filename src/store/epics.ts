import { combineEpics } from 'redux-observable'

import { epics as fileEpics } from './files'

export const rootEpic = combineEpics(fileEpics)
