import { combineReducers } from 'redux'

import { reducer as files, State as FileState } from './files/files.reducer'

export interface RootState {
    files: FileState
}

export const rootReducer = combineReducers<RootState>({ files })
