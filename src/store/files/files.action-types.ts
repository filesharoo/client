export const uploadFile = 'uploadFile'
export const uploadFileError = 'uploadFileError'
export const uploadFileSuccess = 'uploadFileSuccess'
export const noOp = 'noOp'
