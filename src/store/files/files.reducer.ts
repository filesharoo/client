import { RootActions } from '../actions'
import * as types from './files.action-types'

export type State = {
    readonly error: string
    readonly expiresAt: Date | null
    readonly file: File | null
    readonly hash: string
    readonly uploadingFile: boolean
    readonly uri: string
}

const initialState: State = {
    error: '',
    expiresAt: null,
    file: null,
    hash: '',
    uploadingFile: false,
    uri: ''
}

export function reducer(state: State = initialState, action: RootActions): State {
    switch (action.type) {
        case types.uploadFile:
            return { ...initialState, file: action.payload.file, uploadingFile: true }
        case types.uploadFileError:
            return { ...state, error: action.payload.error || '...', uploadingFile: false }
        case types.uploadFileSuccess:
            return {
                ...initialState,
                expiresAt: new Date(action.payload.expiresAt),
                hash: action.payload.hash,
                uri:  action.payload.uri
            }
        default:
            return state
    }
}
