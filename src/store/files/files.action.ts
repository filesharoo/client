import * as types from './files.action-types'
import { FileUploadResponse } from '../../api/files.api'

export type Payloads = {
    noOp: {}
    uploadFile: { file: File }
    uploadFileError: { error: string }
    uploadFileSuccess: FileUploadResponse
}

export type Actions = {
    [P in keyof Payloads]: { payload: Payloads[P], type: P }
}

export type AC<P extends keyof Payloads> = {
    (payload: Payloads[P]): Actions[P]
}

export type ActionCreators = {
    [P in keyof Payloads]: AC<P>
}

export const actionCreators: ActionCreators = {
    noOp: payload => ({ payload, type: types.noOp }),
    uploadFile: payload => ({ payload, type: types.uploadFile }),
    uploadFileError: payload => ({ payload, type: types.uploadFileError }),
    uploadFileSuccess: payload => ({ payload, type: types.uploadFileSuccess })
}
