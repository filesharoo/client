import * as React from 'react'
import { combineEpics, Epic } from 'redux-observable'
import { Observable } from 'rxjs/Observable'
import { Intent } from '@blueprintjs/core'

import * as types from './files.action-types'
import { Actions as FileActions, actionCreators } from './files.action'
import { RootActions } from '../actions'
import { RootState } from '../reducer'
import * as fileAPI from '../../api/files.api'
import { toasterMain } from '../../common'

const uploadFile: Epic<RootActions, RootState> = (action$, store) =>
    action$.ofType(types.uploadFile)
        .map((action: FileActions[typeof types.uploadFile]) => action.payload)
        .switchMap(payload => fileAPI.upload(payload)
            .do(({ uri }) => toasterMain.show({ message: <a href={uri}>{uri}</a> }))
            .map(actionCreators.uploadFileSuccess)
            .catch(error => Observable.of(actionCreators.uploadFileError({ error: error.message }))
                .do(() => toasterMain.show({ intent: Intent.WARNING, message: error.message }))))

export const epics = combineEpics(uploadFile)
