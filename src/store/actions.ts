import { Actions as FileActions } from './files'

export type RootActions =
  | FileActions[keyof FileActions]
